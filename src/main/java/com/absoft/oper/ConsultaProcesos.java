package com.absoft.oper;

import java.io.File;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.absoft.model.Account;
import com.absoft.model.Process;
import com.absoft.services.ConsultarProcesosSeleniumService;
import com.absoft.services.DriverUtils;
import com.absoft.services.db.ProcessDao;
import com.absoft.services.db.express.ProcessService;

public class ConsultaProcesos implements ProcesosAccountObserver {

	private Account account;
	private int hilosActivos = 0;
	ConsultaFirmaObserver observer;

	public ConsultaProcesos(ConsultaFirmaObserver observer, Account account) {
		this.account = account;
		this.observer = observer;
	}

	public void iniciar() {

		final int MAX_NUM = 5;
		int num = MAX_NUM;
		float x = (float) this.account.getLimit() / (float) MAX_NUM;
		int pages = (int) Math.ceil(x);
		List<Process> procesos;
		boolean ask4More = true;
		for (int page = 0; page < pages; page++) {
			if (!ask4More) {
				break;
			}
			if (page == pages - 1) {
				num = this.account.getLimit() % MAX_NUM;
				num = num == 0 ? MAX_NUM : num;
			}
			procesos = cargarProcesos(page, num);
			if (procesos.size() < num) {
				ask4More = false;
			}
			if (procesos.isEmpty()) {
				continue;
			}
			this.hilosActivos++;

			Thread hilo = new Thread(new ConsultorProcesos(this, procesos, this.account));
			hilo.start();
		}
		if (this.hilosActivos == 0) {
			this.observer.procesoFinalizado();
		}
	}

	private List<Process> cargarProcesos(int page, int num) {
		ProcessDao processDao = new ProcessService();
		return processDao.getProcesosByCompanyId(account.getCompanyId(), page, num);
	}

	public synchronized void hiloFinalizado() {

		if (--this.hilosActivos == 0) {
			this.observer.procesoFinalizado();
		}
	}

}

class ConsultorProcesos implements Runnable {
	List<Process> procesos;
	ProcesosAccountObserver observer;
	private Account account;

	public ConsultorProcesos(ProcesosAccountObserver observer, List<Process> procesos, Account account) {
		this.procesos = procesos;
		this.observer = observer;
		this.account = account;
	}

	public void run() {
		try {
			ConsultarProcesosSeleniumService selenium = new ConsultarProcesosSeleniumService(procesos);
			WebDriver driver = DriverUtils.getWebDriver("chrome", new File("descargas"));
			selenium.iniciar(driver);
			driver.quit();
			guardarProcesos(procesos);
		} catch (Exception e) {
			System.out.println("Fall� la compa��a " +  account.getId());
			e.printStackTrace();
		}
	}

	private void guardarProcesos(List<Process> actualizados) {
		ProcessDao processDao = new ProcessService();
		Process[] arrayActualizados = new Process[actualizados.size()];
		arrayActualizados = actualizados.toArray(arrayActualizados);
		processDao.updateProcesos(arrayActualizados);
		this.observer.hiloFinalizado();
	}
}

interface ProcesosAccountObserver {

	void hiloFinalizado();

}
