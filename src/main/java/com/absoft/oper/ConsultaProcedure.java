package com.absoft.oper;

import java.util.List;

import com.absoft.model.Account;
import com.absoft.model.Message;
import com.absoft.oper.interfaces.TaskProcessObserver;
import com.absoft.services.db.CompanyDao;
import com.absoft.services.db.express.CompanyDaoExpress;

public class ConsultaProcedure implements ConsultaFirmaObserver {
	List<Account> accounts;
	int index;
	TaskProcessObserver observer;
	
	public ConsultaProcedure(TaskProcessObserver procesoConsultaObserver) {
		this.observer = procesoConsultaObserver;
	}

	public Message<List<Account>> inciaConsultaGeneralProcesos() {
		CompanyDao companyDao = new CompanyDaoExpress();

		Message<List<Account>> msg = companyDao.getActiveCompanies();
		if (msg.getValid()) {
			accounts = msg.getData();
			index = 0;
			consultarProcesos();
			return msg;
		} else {
			
			return msg;
		}
	}

	private void consultarProcesos() {
		Account account;
		try {
			account = this.accounts.get(this.index);
		} catch (Exception e) {
			observer.consultaCompletada(true, "Se evaluaron " + this.accounts.size() + " firmas, exitosamente.");
			return;
		}
		ConsultaProcesos consulta = new ConsultaProcesos(this, account);
		consulta.iniciar();

	}

	public void procesoFinalizado() {
		this.index++;
		consultarProcesos();
	}

}


