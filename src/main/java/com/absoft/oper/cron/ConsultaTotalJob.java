package com.absoft.oper.cron;

import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.absoft.model.Account;
import com.absoft.model.Message;
import com.absoft.oper.ConsultaProcedure;
import com.absoft.oper.EnumTipoOperacion;
import com.absoft.oper.interfaces.TaskProcessObserver;
import com.absoft.services.db.express.TaskService;

public class ConsultaTotalJob implements Job, TaskProcessObserver {
	String idTask = "";

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		try {
			idTask = TaskService.initTask(EnumTipoOperacion.OP_CONSULTAR_COMPLETO.toString());
			if (this.idTask == null) {
				throw new Exception("No se pudo crear el registro de ejecuci�n.");
			}
			ConsultaProcedure procedimiento = new ConsultaProcedure(this);
			Message<List<Account>> msg = procedimiento.inciaConsultaGeneralProcesos();

		} catch (Exception e) {
			System.out.println("Ocurri� un error con el Job Consulta Total. " + e.getMessage());
		}
	}

	@Override
	public String iniciarTarea(String nombre) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void consultaCompletada(Boolean exito, String mensaje) {
		TaskService.endTask(idTask, exito, mensaje, false);
	}

}
