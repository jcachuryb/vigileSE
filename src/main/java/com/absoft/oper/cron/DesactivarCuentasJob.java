package com.absoft.oper.cron;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.absoft.model.Response;
import com.absoft.oper.EnumTipoOperacion;
import com.absoft.services.db.express.AccountService;
import com.absoft.services.db.express.TaskService;

public class DesactivarCuentasJob implements Job {

	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		String idTask = "";
		try {
			idTask = TaskService.initTask(EnumTipoOperacion.OP_DESACTIVAR.toString());
			AccountService accService = new AccountService();
			Response res = accService.deactivateAccounts();
			System.out.println(res.getMsg());
			TaskService.endTask(idTask, true, res.getMsg(), false);

		} catch (Exception e) {
			TaskService.endTask(idTask, false, e.getMessage(), false);
		}
	}

}
