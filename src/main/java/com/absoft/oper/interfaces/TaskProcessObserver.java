package com.absoft.oper.interfaces;

public interface TaskProcessObserver {

	String iniciarTarea(String nombre);

	void consultaCompletada(Boolean exito, String mensaje);
}
