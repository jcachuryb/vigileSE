package com.absoft.core;

import org.quartz.CronScheduleBuilder;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import com.absoft.oper.cron.ConsultaTotalJob;
import com.absoft.oper.cron.DesactivarCuentasJob;
import com.absoft.oper.cron.TESTJOB;

public class VigileBotApp {

	public static void init() {
		try {
			iniciarCron(new ConsultaTotalJob(), VigileBotProperties.timer_consulta_procesos);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			iniciarCron(new DesactivarCuentasJob(), VigileBotProperties.timer_consulta_cuentas);
		} catch (SchedulerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void iniciarCron(Job job, String scheduleTime) throws SchedulerException {

		JobDetail jobDetail = JobBuilder.newJob(job.getClass()).build();

		Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronSchedule(scheduleTime))
				.build();

		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();
		scheduler.scheduleJob(jobDetail, trigger);

	}
}