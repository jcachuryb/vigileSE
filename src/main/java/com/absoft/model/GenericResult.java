package com.absoft.model;

import com.google.gson.annotations.SerializedName;

public class GenericResult {

	@SerializedName("data")
	private String data;

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
