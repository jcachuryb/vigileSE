package com.absoft.model;

public class ProcessData {
	private String codProceso;
	private String ciudad;
	private String entidad;

	public String getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	@Override
	public String toString() {
		return "ProcessData [ciudad=" + ciudad + ", entidad=" + entidad
				+ ", codProceso=" + codProceso + "]";
	}

}
