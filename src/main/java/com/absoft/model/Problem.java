package com.absoft.model;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class Problem extends ParseModel {

	@SerializedName("msg")
	private String message;
	@SerializedName("date")
	private Date date;

	public Problem() {
		this.message = "Mensaje de error general";
		this.date = new Date();
	}

	public Problem(String message) {
		this.message = message;
		this.date = new Date();
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
