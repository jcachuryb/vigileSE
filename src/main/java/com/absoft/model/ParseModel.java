package com.absoft.model;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class ParseModel {

	@SerializedName("objectId")
	private String id;
	@SerializedName("createdAt")
	private String createdAt;
	@SerializedName("updatedAt")
	private String updatedAt;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(String updatedAt) {
		this.updatedAt = updatedAt;
	}

}
