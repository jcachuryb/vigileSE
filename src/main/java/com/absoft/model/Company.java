package com.absoft.model;

import com.google.gson.annotations.SerializedName;

public class Company extends ParseModel {

	@SerializedName("nombre")
	private String name;
	@SerializedName("NIT")
	private String nit;
	@SerializedName("telefonos")
	private String[] telefonos;
	@SerializedName("cuenta")
	private Account cuenta;
	@SerializedName("usuarios")
	private String[] usuarios;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String[] getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String[] telefonos) {
		this.telefonos = telefonos;
	}

	public Account getCuenta() {
		return cuenta;
	}

	public void setCuenta(Account cuenta) {
		this.cuenta = cuenta;
	}

}
