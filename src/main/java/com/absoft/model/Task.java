package com.absoft.model;

import java.util.Date;

public class Task {

	private String tipo;
	private Date inicio;
	private Date fin;
	private Boolean exito;
	private String comentarios;
	private Boolean descartar;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}

	public Boolean getExito() {
		return exito;
	}

	public void setExito(Boolean exito) {
		this.exito = exito;
	}

	public String getComentarios() {
		return comentarios;
	}

	public void setComentarios(String comentarios) {
		this.comentarios = comentarios;
	}

	public Boolean getDescartar() {
		return descartar;
	}

	public void setDescartar(Boolean descartar) {
		this.descartar = descartar;
	}

}
