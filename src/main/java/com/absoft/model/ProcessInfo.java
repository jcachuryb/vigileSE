package com.absoft.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ProcessInfo {
	@SerializedName("despacho")
	private String despacho;
	@SerializedName("ponente")
	private String ponente;
	@SerializedName("clasificacion")
	private ProcessClass clasificacion;
	@SerializedName("demandantes")
	private List<String> demandantes;
	@SerializedName("demandados")
	private List<String> demandados;
	@SerializedName("contenido")
	private String contenido;

	public String getDespacho() {
		return despacho;
	}

	public void setDespacho(String despacho) {
		this.despacho = despacho;
	}

	public String getPonente() {
		return ponente;
	}

	public void setPonente(String ponente) {
		this.ponente = ponente;
	}

	public ProcessClass getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(ProcessClass clasificacion) {
		this.clasificacion = clasificacion;
	}

	public List<String> getDemandantes() {
		return demandantes;
	}

	public void setDemandantes(List<String> demandantes) {
		this.demandantes = demandantes;
	}

	public List<String> getDemandados() {
		return demandados;
	}

	public void setDemandados(List<String> demandados) {
		this.demandados = demandados;
	}

	public String getContenido() {
		return contenido;
	}

	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!ProcessInfo.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		final ProcessInfo other = (ProcessInfo) obj;
		if ((this.despacho == null) ? (other.despacho != null) : !this.despacho
				.equals(other.despacho)) {
			return false;
		}
		if ((this.ponente == null) ? (other.ponente != null) : !this.ponente
				.equals(other.ponente)) {
			return false;
		}
		if ((this.clasificacion == null) ? (other.clasificacion != null)
				: !this.clasificacion.equals(other.clasificacion)) {
			return false;
		}
		if ((this.contenido == null) ? (other.contenido != null)
				: !this.contenido.equals(other.contenido)) {
			return false;
		}
		if ((this.demandados == null) ? (other.demandados != null)
				: !this.demandados.equals(other.demandados)) {
			return false;
		}
		if ((this.demandantes == null) ? (other.demandantes != null)
				: !this.demandantes.equals(other.demandantes)) {
			return false;
		}
		return true;
	}

}
