package com.absoft.model;

import com.google.gson.annotations.SerializedName;

public class ProcessClass {
	@SerializedName("tipo")
	private String tipo;
	@SerializedName("clase")
	private String clase;
	@SerializedName("recurso")
	private String recurso;
	@SerializedName("ubicacion")
	private String ubicacion;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getClase() {
		return clase;
	}

	public void setClase(String clase) {
		this.clase = clase;
	}

	public String getRecurso() {
		return recurso;
	}

	public void setRecurso(String recurso) {
		this.recurso = recurso;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!ProcessClass.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		final ProcessClass other = (ProcessClass) obj;
		if ((this.clase == null) ? (other.clase != null) : !this.clase
				.equals(other.clase)) {
			return false;
		}
		if ((this.recurso == null) ? (other.recurso != null) : !this.recurso
				.equals(other.recurso)) {
			return false;
		}
		if ((this.tipo == null) ? (other.tipo != null) : !this.tipo
				.equals(other.tipo)) {
			return false;
		}
		if ((this.ubicacion == null) ? (other.ubicacion != null)
				: !this.ubicacion.equals(other.ubicacion)) {
			return false;
		}

		return true;
	}

}
