package com.absoft.model;

import java.util.Date;
import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Process extends ParseModel {

	@SerializedName("codProceso")
	private String codProceso;
	@SerializedName("ciudad")
	private String ciudad;
	@SerializedName("entidad")
	private String entidad;
	@SerializedName("estadoActual")
	private State estadoActual;
	@SerializedName("indexActuacion")
	private Integer indexActuacion = 0;
	@SerializedName("data")
	private ProcessData data;
	@SerializedName("problem")
	private Problem problem;
	@SerializedName("info")
	private ProcessInfo info;
	@SerializedName("reconsultar")
	private Boolean reconsultar;
	@SerializedName("tags")
	private List<String> tags;
	@SerializedName("ultimaAct")
	private Date ultimaAct;

	// UPDATE FLAGS
	@SerializedName("alerta")
	private String alerta;
	private Boolean actualizado = false;
	private Boolean infoActualizada = false;
	private Boolean estadoActualizado = false;
	private Boolean estadosActualizados = false;
	private Boolean repetirScan = false;

	public String getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getEntidad() {
		return entidad;
	}

	public void setEntidad(String entidad) {
		this.entidad = entidad;
	}

	public State getEstadoActual() {
		return estadoActual;
	}

	public void setEstadoActual(State estadoActual) {
		this.estadoActual = estadoActual;
	}

	public Integer getIndexActuacion() {
		return indexActuacion;
	}

	public void setIndexActuacion(Integer indexActuacion) {
		this.indexActuacion = indexActuacion;
	}

	public ProcessData getData() {
		return data;
	}

	public void setData(ProcessData data) {
		this.data = data;
	}

	public Problem getProblem() {
		return problem;
	}

	public void setProblem(Problem problem) {
		this.problem = problem;
	}

	public ProcessInfo getInfo() {
		return info;
	}

	public void setInfo(ProcessInfo info) {
		this.info = info;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public Boolean getActualizado() {
		return actualizado;
	}

	public Boolean getEstadoActualizado() {
		return estadoActualizado;
	}

	public void actualizado() {
		this.actualizado = true;
	}

	public void infoActualizada() {
		this.infoActualizada = true;
	}

	public void estadoActualizado() {
		this.estadoActualizado = true;
	}

	public void estadosActualizados() {
		this.estadosActualizados = true;
	}

	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}

	public String getAlerta() {
		return this.alerta;
	}

	public Boolean getRepetirScan() {
		return repetirScan;
	}

	public void setRepetirScan(Boolean repetirScan) {
		this.repetirScan = repetirScan;
	}

	public Boolean getReconsultar() {
		return reconsultar;
	}

	public void setReconsultar(Boolean reconsultar) {
		this.reconsultar = reconsultar;
	}

	public Date getUltimaAct() {
		return ultimaAct;
	}

	public void setUltimaAct(Date ultimaAct) {
		this.ultimaAct = ultimaAct;
	}

}
