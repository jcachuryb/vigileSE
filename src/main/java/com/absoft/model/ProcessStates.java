package com.absoft.model;

import com.google.gson.annotations.SerializedName;

public class ProcessStates {

	@SerializedName("objectId")
	private String id;
	@SerializedName("proceso")
	private String processId;
	@SerializedName("html")
	private String html;
	@SerializedName("total")
	private Integer total;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessId() {
		return processId;
	}

	public void setProcessId(String processId) {
		this.processId = processId;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
