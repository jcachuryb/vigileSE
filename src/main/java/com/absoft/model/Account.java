package com.absoft.model;

import java.util.Date;

import com.google.gson.annotations.SerializedName;

public class Account extends ParseModel {

	@SerializedName("activation")
	private Date activation;
	@SerializedName("ends")
	private Date ends;

	@SerializedName("active")
	private Boolean active;
	@SerializedName("companyId")
	private String companyId;
	@SerializedName("limit")
	private Integer limit;

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public String getCompanyId() {
		return companyId;
	}

	public void setCompanyId(String companyId) {
		this.companyId = companyId;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Date getActivation() {
		return activation;
	}

	public void setActivation(Date activation) {
		this.activation = activation;
	}

	public Date getEnds() {
		return ends;
	}

	public void setEnds(Date ends) {
		this.ends = ends;
	}

}
