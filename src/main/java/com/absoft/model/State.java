package com.absoft.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.gson.annotations.SerializedName;

public class State {
	@SerializedName("codProceso")
	private String codProceso;
	@SerializedName("actuacion")
	private String actuacion;
	@SerializedName("fechaActuacion")
	private String fechaActuacion;
	@SerializedName("anotacion")
	private String anotacion;
	@SerializedName("fechaInicioTermino")
	private String fechaInicioTermino;
	@SerializedName("fechaFinTermino")
	private String fechaFinTermino;
	@SerializedName("fechaRegistro")
	private String fechaRegistro;
	@SerializedName("num")
	private Integer num;

	private Date ultimaAct;

	public String getCodProceso() {
		return codProceso;
	}

	public void setCodProceso(String codProceso) {
		this.codProceso = codProceso;
	}

	public String getActuacion() {
		return actuacion;
	}

	public void setActuacion(String actuacion) {
		this.actuacion = actuacion;
	}

	public String getAnotacion() {
		return anotacion;
	}

	public void setAnotacion(String anotacion) {
		this.anotacion = anotacion;
	}

	public String getFechaActuacion() {
		return fechaActuacion;
	}

	public void setFechaActuacion(String fechaActuacion) {
		this.fechaActuacion = fechaActuacion;
	}

	public String getFechaInicioTermino() {
		return fechaInicioTermino;
	}

	public void setFechaInicioTermino(String fechaInicioTermino) {
		this.fechaInicioTermino = fechaInicioTermino;
	}

	public String getFechaFinTermino() {
		return fechaFinTermino;
	}

	public void setFechaFinTermino(String fechaFinTermino) {
		this.fechaFinTermino = fechaFinTermino;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}

	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}

	public Date parseFechaActuacion() {
		String input = this.fechaActuacion;
		if (input == null || input.equals("")) {
			return null;
		}
		DateFormat format = new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH);
		DateFormat outputFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
		try {
			return outputFormat.parse(input);
		} catch (ParseException e) {
			System.err.println("No pudo procesar la fecha ingresada en espa�ol: " + input);
			try {
				return format.parse(input);
			} catch (ParseException e1) {
				System.err.println("No pudo procesar la fecha ingresada en ingl�s: " + input);
				System.err.println("Nos jodimos");
				return null;
			}
		}

	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!State.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		final State other = (State) obj;
		if ((this.actuacion == null) ? (other.actuacion != null) : !this.actuacion.equals(other.actuacion)) {
			return false;
		}
		if ((this.anotacion == null) ? (other.anotacion != null) : !this.anotacion.equals(other.anotacion)) {
			return false;
		}
		if ((this.fechaActuacion == null) ? (other.fechaActuacion != null)
				: !this.fechaActuacion.equals(other.fechaActuacion)) {
			return false;
		}
		if ((this.fechaInicioTermino == null) ? (other.fechaInicioTermino != null)
				: !this.fechaInicioTermino.equals(other.fechaInicioTermino)) {
			return false;
		}
		if ((this.fechaFinTermino == null) ? (other.fechaFinTermino != null)
				: !this.fechaFinTermino.equals(other.fechaFinTermino)) {
			return false;
		}
		if ((this.fechaRegistro == null) ? (other.fechaRegistro != null)
				: !this.fechaRegistro.equals(other.fechaRegistro)) {
			return false;
		}
		return true;
	}
}
