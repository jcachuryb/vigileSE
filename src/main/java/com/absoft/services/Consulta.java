package com.absoft.services;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Consulta {

	static String ciudad = "BOGOTA, D.C.";
	static String entidad = "499-True-3333-11001-Juzgado Administrativo-Oralidad";
	static String tipoConsulta = "1"; // N�mero de Radicaci�n
	static String idProceso = "11001333100320070018600";

	public static void main(String[] args) {

		// String chromePath =
		// "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
		// System.setProperty("webdriver.chrome.driver", chromePath);
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Mozilla Firefox\\firefox.exe");

		WebDriver driver = new FirefoxDriver();

		driver.manage().window().setSize(new Dimension(1024, 768));

		driver.get("http://procesos.ramajudicial.gov.co/consultaprocesos/");

		Select select;
		// Type in the search-field: "WebDriver"
		select = new Select(driver.findElement(By.id("ddlCiudad")));
		select.selectByVisibleText(ciudad);

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		select = new Select(waitForElements(driver, By.id("ddlEntidadEspecialidad")));
		
		select.selectByValue(entidad);

		select = new Select(waitForElements(driver, By.id("rblConsulta")));

		select.selectByValue(tipoConsulta);

		WebElement divRadicacion = driver
				.findElement(By.id("divNumRadicacion"));

		WebElement inputId = divRadicacion.findElement(By
		//		.xpath("//input[@type='text']"));
				.xpath("//*[@id='divNumRadicacion']/table/tbody/tr[2]/td/div/input"));
		inputId.sendKeys(idProceso);
		WebElement btnConsultar;
		btnConsultar = divRadicacion.findElement(By
				.xpath("//input[@value='Consultar']"));
		WebElement slider;
		slider = divRadicacion
				.findElement(By
						.xpath("//*[@id='sliderBehaviorNumeroProceso_railElement']/div"));

		Actions move = new Actions(driver);
		Action action = (Action) move.dragAndDropBy(slider, 30, 0).build();
		action.perform();

		btnConsultar.click();

		// BUSCAR EL RESULTADO
		WebElement myDynamicElement = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.presenceOfElementLocated(By
						.id("rptActuaciones_lblActuacion_0")));
		System.out.println("Estado proceso: " +  myDynamicElement.getText());
		


	}
	 

	public static WebElement waitForElements(WebDriver driver, By by){
		return (new WebDriverWait(driver, 30))
		.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public boolean waitForPageLoad(WebDriver driver, int waitTimeInSec,
			ExpectedCondition<Boolean>... conditions) {
		boolean isLoaded = false;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(waitTimeInSec, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class)
				.pollingEvery(2, TimeUnit.SECONDS);
		for (ExpectedCondition<Boolean> condition : conditions) {
			isLoaded = wait.until(condition);
			if (isLoaded == false) {
				// Stop checking on first condition returning false.
				break;
			}
		}
		return isLoaded;
	}

	/**
	 * Returns 'true' if the value of the 'window.document.readyState' via
	 * JavaScript is 'complete'
	 */
	public static final ExpectedCondition<Boolean> EXPECT_DOC_READY_STATE = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			String script = "if (typeof window != 'undefined' && window.document) { return window.document.readyState; } else { return 'notready'; }";
			Boolean result;
			try {
				result = ((JavascriptExecutor) driver).executeScript(script)
						.equals("complete");
			} catch (Exception ex) {
				result = Boolean.FALSE;
			}
			return result;
		}
	};

}
