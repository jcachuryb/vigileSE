package com.absoft.services;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Cualquiera {
	public static void main(String[] args) {

		// String chromePath =
		// "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
		// System.setProperty("webdriver.chrome.driver", chromePath);

		WebDriver driver = new FirefoxDriver();

		driver.manage().window().setSize(new Dimension(1024, 768));

		driver.get("http://procesos.ramajudicial.gov.co/consultaprocesos/");

		Select ciudad;
		Select entidad;

		ciudad = new Select(driver.findElement(By.id("ddlCiudad")));
		entidad = null;

		List<String> ciudades = getCiudades(driver);
		
		System.out.println("[");
		for (String cityValue : ciudades) {
			if (cityValue.equals("0"))
				continue;

			ciudad = new Select(driver.findElement(By.id("ddlCiudad")));
			ciudad.selectByValue(cityValue);
			waitMofo(driver, "ddlCiudad");
			
			ciudad = new Select(driver.findElement(By.id("ddlCiudad")));
			String text = ciudad.getFirstSelectedOption().getText();
			System.out.println("{nombre: '" + text + "', value: '" + cityValue
					+ "',  entidades: [");

			entidad = new Select(driver.findElement(By
					.id("ddlEntidadEspecialidad")));
			List<WebElement> entidades = entidad.getOptions();
			for (WebElement e : entidades) {
				if (e.equals("0"))
					continue;
				String v = e.getAttribute("value");
				String t = e.getText();
				System.out.println("{ nombre: '" + t + "' , valor: '" + v
						+ "'},");
			}
			System.out.println("]}, ");
			entidad = null;
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
		System.out.println("]");

	}

	public static void waitAndSelectByValue(WebDriver driver, String element,
			String value) {
		// WebDriverWait wait = new WebDriverWait(driver, 10);
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ddlEntidadEspecialidad")));
		waitMofo(driver, null);
		Select s = new Select(driver.findElement(By.id(element)));
		waitMofo(driver, null);
		// wait.until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
		s.selectByValue(value);
	}

	private static void waitMofo(WebDriver driver, String element) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<String> getCiudades(WebDriver driver) {
		List<String> ciudades = new ArrayList<String>();
		Select ciudad = new Select(driver.findElement(By.id("ddlCiudad")));
		List<WebElement> options = ciudad.getOptions();
		for (WebElement option : options) {
			String value = option.getAttribute("value");
			String text = option.getText();
			ciudades.add(value);
		}

		return ciudades;
	}

	public static WebElement waitForElements(WebDriver driver, By by) {
		return (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.presenceOfElementLocated(by));
	}

	public boolean waitForPageLoad(WebDriver driver, int waitTimeInSec,
			ExpectedCondition<Boolean>... conditions) {
		boolean isLoaded = false;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(waitTimeInSec, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class)
				.pollingEvery(2, TimeUnit.SECONDS);
		for (ExpectedCondition<Boolean> condition : conditions) {
			isLoaded = wait.until(condition);
			if (isLoaded == false) {
				// Stop checking on first condition returning false.
				break;
			}
		}
		return isLoaded;
	}

	/**
	 * Returns 'true' if the value of the 'window.document.readyState' via
	 * JavaScript is 'complete'
	 */
	public static final ExpectedCondition<Boolean> EXPECT_DOC_READY_STATE = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			String script = "if (typeof window != 'undefined' && window.document) { return window.document.readyState; } else { return 'notready'; }";
			Boolean result;
			try {
				result = ((JavascriptExecutor) driver).executeScript(script)
						.equals("complete");
			} catch (Exception ex) {
				result = Boolean.FALSE;
			}
			return result;
		}
	};
}
