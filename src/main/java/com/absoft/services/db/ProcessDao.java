package com.absoft.services.db;

import java.util.List;

import com.absoft.model.GenericResult;
import com.absoft.model.Process;
import com.absoft.model.State;

public interface ProcessDao {
	public List<Process> getProcesosByCompanyId(String companyId, int page, int num);

	public GenericResult getInfoEstadosProceso(String codProceso);

	public void updateProcesos(Process[] procesos);

	public Boolean saveProcessStateBatch(State[] loteEstados);

}
