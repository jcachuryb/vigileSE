package com.absoft.services.db;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import com.absoft.core.VigileBotProperties;
import com.absoft.core.VigileBotProperties.Property;
import com.absoft.model.Message;
import com.absoft.model.Process;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

public class ParseServer {

	private static ParseServer server;
	private static String serverURL;

	public static void initialize() {
		serverURL = VigileBotProperties.getProperty(Property.SERVER_URL);
	}

	public static <T> Message<T> postToServer(String path, JSONObject params, Class<T> outputClass) {
		String paramsString = params != null ? params.toString() : "{}";
		try {
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30 * 1000).build();
			HttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
			HttpPost post = new HttpPost(serverURL + path);
			post.setHeader("Content-type", "application/json");
			post.setHeader("Accept", "application/json");
			post.setEntity(new StringEntity(paramsString, "UTF-8"));
			HttpResponse response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				Reader reader = new InputStreamReader(in, "UTF-8");
				if (outputClass == null) {
					return null;
				}
				Gson gSon = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
				return new Message<T>(true, "OK", new Gson().fromJson(reader, outputClass));
			}
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
			ex.printStackTrace();
			return new Message<>(false, ex.getMessage());
		}
		return new Message<>(false, "No hubo respuesta");
	}

	// public static <T> T postToServerMultipart(String path, JSONObject params,
	// Class<T> outputClass) {
	// CloseableHttpClient httpClient = HttpClients.createDefault();
	// HttpPost uploadFile = new HttpPost("...");
	// MultipartEntityBuilder builder = MultipartEntityBuilder.create();
	// builder.addTextBody("field1", "yes", ContentType.TEXT_PLAIN);
	//
	// // This attaches the file to the POST:
	// File f = new File("[/path/to/upload]");
	// builder.addBinaryBody(
	// "file",
	// new FileInputStream(f),
	// ContentType.APPLICATION_OCTET_STREAM,
	// f.getName()
	// );
	//
	// HttpEntity multipart = builder.build();
	// uploadFile.setEntity(multipart);
	// CloseableHttpResponse response = httpClient.execute(uploadFile);
	// HttpEntity responseEntity = response.getEntity();
	// }

	public static void getCompanies() {
		HttpClient httpClient = HttpClientBuilder.create().build(); // Use this
																	// instead
		JSONObject json = new JSONObject();
		json.put("time_range", "22-23");
		json.put("flow_id", "786");
		json.put("ip_a", "192.65.78.22");
		json.put("port_a", "8080");
		json.put("regex", "%ab");

		try {
			RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(30 * 1000).build();
			HttpClient client = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
			HttpPost post = new HttpPost("http://localhost:3000/api/bot/load");
			post.setHeader("Content-type", "application/json");
			post.setHeader("Accept", "application/json");
			JSONObject obj = new JSONObject();
			obj.put("username", "abcd");
			obj.put("password", "1234");
			post.setEntity(new StringEntity(obj.toString(), "UTF-8"));
			HttpResponse response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				Reader reader = new InputStreamReader(in, "UTF-8");
				Process[] p = new Gson().fromJson(reader, Process[].class);
				for (int i = 0; i < p.length; i++) {
					System.out.println(p[i].getCodProceso());
				}

			}
			// handle response here...

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			// Deprecated
			// httpClient.getConnectionManager().shutdown();
		}
	}

}

class BotParams {
	@SerializedName("name")
	private String name;
	@SerializedName("age")
	private String age;

	public BotParams(String name, String age) {
		super();
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "BotParams [name=" + name + ", age=" + age + "]";
	}

}
