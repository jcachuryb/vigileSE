package com.absoft.services.db;

public interface TaskDao {
	
	public String initTask(String tipo);

	public Boolean endTask(String id, Boolean exito, String comentarios, Boolean descartar);

}
