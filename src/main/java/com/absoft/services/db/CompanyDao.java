package com.absoft.services.db;

import java.util.List;

import com.absoft.model.Account;
import com.absoft.model.Message;

public interface CompanyDao {

	public Message<List<Account>> getActiveCompanies();
	
}
