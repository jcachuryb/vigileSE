package com.absoft.services.db.express;

import org.json.JSONObject;

import com.absoft.model.Message;
import com.absoft.model.Response;
import com.absoft.services.db.ParseServer;

public class AccountService {

	public Response deactivateAccounts() {

		JSONObject params = new JSONObject();
		try {
			Message<Response> message = ParseServer.postToServer("bot/deactivateAccounts", params, Response.class);
			if (message.getValid()) {
				return message.getData();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Response warnAlmostDueAccounts() {

		JSONObject params = new JSONObject();
		try {
			Message<Response> message = ParseServer.postToServer("bot/warnAlmostDueAccounts", params, Response.class);
			if (message.getValid()) {
				return message.getData();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public Response activateAccounts() {

		JSONObject params = new JSONObject();
		try {
			Message<Response> message = ParseServer.postToServer("bot/loadDueAccounts", params, Response.class);
			if (message.getValid()) {
				return message.getData();
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

}
