package com.absoft.services.db.express;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.absoft.model.Account;
import com.absoft.model.Message;
import com.absoft.services.db.CompanyDao;
import com.absoft.services.db.ParseServer;

public class CompanyDaoExpress implements CompanyDao {

	public Message<List<Account>> getActiveCompanies() {

		Message<List<Account>> message = new Message<List<Account>>();

		try {
//			Account[] companies = 
			Message<Account[]> msg = ParseServer.postToServer("bot/loadCompanies", null, Account[].class);

			if (msg.getValid()) {
				List<Account> list = new ArrayList<Account>(Arrays.asList(msg.getData()));
				message.setData(list);
			}
			message.setValid(msg.getValid());
			message.setMsg(msg.getMsg());

		} catch (Exception e) {
		}

		return message;
	}

	public Message<Account> getCompanyById(String companyId) {

		return new Message<>(false, "No disponible");
	}

}
