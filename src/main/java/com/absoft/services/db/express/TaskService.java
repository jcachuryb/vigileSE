package com.absoft.services.db.express;

import org.json.JSONObject;

import com.absoft.services.db.ParseServer;

public class TaskService {

	public static String initTask(String tipo) {
		JSONObject params = new JSONObject();
		params.put("tipo", tipo);
		try {

			return ParseServer.postToServer("bot/initTask", params, String.class).getData();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	public static void endTask(String id, Boolean exito, String comentarios, Boolean descartar) {

		if (id == null) {
			return;
		}

		JSONObject params = new JSONObject();

		params.put("id", id);
		params.put("exito", exito);
		params.put("comentarios", comentarios);
		params.put("descartar", descartar);

		try {

			ParseServer.postToServer("bot/endTask", params, Object.class);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
