package com.absoft.services.db.express;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

import com.absoft.model.GenericResult;
import com.absoft.model.Message;
import com.absoft.model.Process;
import com.absoft.model.State;
import com.absoft.services.ConsultarProcesosSeleniumService;
import com.absoft.services.DriverUtils;
import com.absoft.services.db.ParseServer;
import com.absoft.services.db.ProcessDao;
import com.google.gson.Gson;

public class ProcessService implements ProcessDao {

	public Message<String> validarProceso(Process proceso) {
		JSONObject params = new JSONObject();
		try {
			List<Process> procesos = new ArrayList<>();

			procesos.add(proceso);
			WebDriver driver = DriverUtils.getWebDriver("chrome", new File("descargas"));
			ConsultarProcesosSeleniumService botSE = new ConsultarProcesosSeleniumService(procesos);
			Message<String> valido = botSE.validarProceso(driver, proceso);

			driver.quit();

			params.put("valido", valido.getValid());
			params.put("mensaje", valido.getMsg());
			return new Message<String>(true, "OK", params.toString());
		} catch (Exception e) {
			params.put("valido", false);
			params.put("mensaje", "Ocurri� un error y  no se pudo validar el proceso.");
			return new Message<>(false, "Ocurri� un error", params.toString());
		}
	}

	public List<Process> getProcesosByCompanyId(String companyId, int page, int num) {

		JSONObject params = new JSONObject();
		params.put("company", companyId);
		params.put("page", page);
		params.put("num", num);
//		Process[] companies = 
		Message<Process[]> msg = ParseServer.postToServer("bot/loadCompanyProcesses", params, Process[].class);
		if (msg.getValid()) {

			return new ArrayList<Process>(Arrays.asList(msg.getData()));
		}

		return new ArrayList<>();
	}

	public GenericResult getInfoEstadosProceso(String codProceso) {

		JSONObject params = new JSONObject();
		params.put("codProceso", codProceso);
		Message<GenericResult> msg = ParseServer.postToServer("bot/getInfoProcessStatus", params, GenericResult.class);
		if (msg.getValid()) {
			return msg.getData();
		}
		return null;
	}

	public void updateProcesos(Process[] procesos) {

		JSONObject params = new JSONObject();
		params.put("procesos", new Gson().toJson(procesos));
		Message<String> msg = ParseServer.postToServer("bot/updateProcesses", params, String.class);
		if (msg.getValid()) {
			System.out.println("Exito al guardar procesos " + procesos.length);
		} else {
			System.out.println("Exito al guardar: " + msg.getMsg());
		}

	}

	public Boolean saveProcessStateBatch(State[] loteEstados) {
		JSONObject params = new JSONObject();
		params.put("estados", new Gson().toJson(loteEstados));
		return ParseServer.postToServer("bot/saveMultipleStatus", params, Boolean.class).getValid();
	}

}
