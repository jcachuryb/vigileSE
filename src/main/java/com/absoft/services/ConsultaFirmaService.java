package com.absoft.services;

import com.absoft.model.Account;
import com.absoft.oper.ConsultaFirmaObserver;
import com.absoft.oper.ConsultaProcesos;

public class ConsultaFirmaService {

	public static void consultarFirma(String id_firma) {
		ConsultaFirmaObserver observer = new ConsultaFirmaObserver() {

			@Override
			public void procesoFinalizado() {
				// TODO Auto-generated method stub

			}
		};
		Account account = new Account();
		account.setActive(true);
		account.setCompanyId(id_firma);
		account.setLimit(100);
		ConsultaProcesos consulta = new ConsultaProcesos(observer, account);
		consulta.iniciar();
	}

}
