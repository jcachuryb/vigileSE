package com.absoft.services;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.absoft.model.Message;
import com.absoft.model.Problem;
import com.absoft.model.Process;
import com.absoft.model.ProcessClass;
import com.absoft.model.ProcessData;
import com.absoft.model.ProcessInfo;
import com.absoft.model.State;
import com.absoft.services.db.ProcessDao;
import com.absoft.services.db.express.ProcessService;
import com.absoft.utils.PageUtils;
import com.google.common.base.Predicate;

public class ConsultarProcesosSeleniumService {

	private WebDriver driver;
	private List<Process> procesos;
	private final String webPage = "http://procesos.ramajudicial.gov.co/consultaprocesos/";

	private static final String campoCiudad = "ddlCiudad";
	private static final String campoEntidad = "ddlEntidadEspecialidad";
	private static final String campoTipoConsulta = "rblConsulta";
	private static final String modalErrorDiv = "modalError";
	private static final String modalErrorMessage = "msjError";
	private static final String btnNuevaConsulta = "btnNuevaConsultaNum";

	// COLUMNS OF STATES
	public static final String lblFechaActuacion = "rptActuaciones_lblFechaActuacion_";
	public static final String lblActuacion = "rptActuaciones_lblActuacion_";
	public static final String lblAnotacion = "rptActuaciones_lblAnotacion_";
	public static final String lblFechaInicio = "rptActuaciones_lblFechaInicio_";
	public static final String lblFechaFin = "rptActuaciones_lblFechaFin_";
	public static final String lblFechaRegistro = "rptActuaciones_lblFechaRegistro_";

	public Map<String, Map<String, Integer>> mapaEntidades = new HashMap<String, Map<String, Integer>>();

	public ConsultarProcesosSeleniumService(List<Process> procesos) {
		this.procesos = procesos;
	}

	@SuppressWarnings("unchecked")
	public List<Process> iniciar(WebDriver wd) {
		this.driver = wd;
		// driver.manage().window().setSize(new Dimension(1024, 768));
		driver.get(webPage);
		waitForPageLoad(driver, 2);
		for (Process proceso : procesos) {
			try {
				goForNuevaConsulta();
			} catch (Exception e) {
				driver.navigate().refresh();
			}
			scanProceso(proceso);
		}

		driver.close();
		return procesos;
	}

	private void scanProceso(Process proceso) {

		ProcessDao processDao = new ProcessService();
		try {
			goForNuevaConsulta();
		} catch (Exception e) {
			driver.navigate().refresh();
		}

		try {
			cargarDatos(proceso);
			if (proceso.getProblem() != null || proceso.getAlerta() != null) {
				return;
			}
			if (hayProblema(revisarInfo(proceso), proceso)) {
				return;
			}
			if (hayProblema(revisarUltimoEstado(proceso), proceso)) {
				return;
			}
			if (hayProblema(obtenerEstados(proceso), proceso)) {
				return;
			}
		} catch (Exception e) {
			reload();
		}
	}

	private boolean hayProblema(String problema, Process proceso) {
		if (problema.length() > 0) {
			Problem problem = new Problem();
			problem.setDate(new Date());
			problem.setMessage(problema);
			proceso.setProblem(problem);
			return true;
		}
		return false;
	}

	private void cargarDatos(Process process) {
		ProcessData data = process.getData();
		boolean hayResultado = false;
		int contador = 0;
		String error = "";
		Select select;

		while (!hayResultado && contador < 30) {

			try {
				if (waitForElements(driver, By.id(campoCiudad)) != null) {

					select = new Select(driver.findElement(By.id(campoCiudad)));
					waitUntilSelectOptionsPopulated(new Select(driver.findElement(By.id(campoCiudad))));
					if (select.getOptions().size() <= 1) {
						driver.get(webPage);
						continue;
					}
					select.selectByValue(data.getCiudad());

					// Waits until campoEntidad options are availabe
					waitUntilSelectOptionsPopulated(new Select(driver.findElement(By.id(campoEntidad))));
					if (waitForElements(driver, By.id(campoEntidad)) != null) {
						select = new Select(driver.findElement(By.id(campoEntidad)));

						// Cargar mapa con valores
						cargarListaOpciones(data.getCiudad(), select.getOptions());
						int index = getEntidadIndexInSelect(data.getCiudad(), data.getEntidad());
						// select.selectByValue(data.getEntidad());
						select.selectByIndex(index);
						break;
					}
				}

			} catch (Exception e) {
				if (e.getMessage().contains("Cannot locate option with value")) {
					error = " No encuentra el valor " + e.getMessage().split(": ")[1];
					process.setAlerta(
							"La entidad consultada presenta inconvenientes y no est� habilitada para su consulta.");
					hayResultado = true;
					return;
				}
			}
			contador++;
		}
		select = new Select(waitForElements(driver, By.id(campoTipoConsulta)));
		waitUntilSelectOptionsPopulated(select);

		select.selectByValue("1");
		WebElement divRadicacion = driver.findElement(By.id("divNumRadicacion"));

		WebElement inputId = divRadicacion
				.findElement(By.xpath("//*[@id='divNumRadicacion']/table/tbody/tr[2]/td/div/input"));

		inputId.clear();
		inputId.sendKeys(data.getCodProceso());
		// inputId.sendKeys("12345678912345678912345");

		WebElement btnConsultar;
		btnConsultar = divRadicacion.findElement(By.xpath("//input[@value='Consultar']"));
		WebElement slider;
		slider = divRadicacion.findElement(By.xpath("//*[@id='sliderBehaviorNumeroProceso_railElement']/div"));

		Actions move = new Actions(driver);
		Action action = (Action) move.dragAndDropBy(slider, 30, 0).build();
		action.perform();
		btnConsultar.click();

		if (waitForModalError(driver)) {
			error = driver.findElement(By.id(modalErrorMessage)).getText();
			if (error.contains("La entidad consultada presenta inconvenientes")) {
				process.setAlerta(error);
				return;
			}
			hayProblema(error, process);
			driver.findElement(By.xpath("//*[@id='" + modalErrorDiv + "']/div/table/tbody/tr/td/input")).click();
			return;
		}
		String m = "La entidad consultada presenta inconvenientes t�cnicos por favor consulte mas tarde. Si persiste la falla por favor dirigirse al despacho";

		if (!waitForResults(driver)) {
			error = "Ocurri� un error y no se mostraron los resultados. ";
		}

	}

	private String obtenerEstados(Process proceso) {
		ProcessDao processDao = new ProcessService();
		Integer numActuaciones = 0;
		int total = driver
				.findElements(By.xpath(
						"//*[@id='divActuacionesDetalle']/table/tbody/tr[2]/td/table/tbody/tr[@class='tr_contenido']"))
				.size();

		proceso.setIndexActuacion(total);
		if (proceso.getEstadoActualizado()) {
			try {
				numActuaciones = Integer.parseInt(processDao.getInfoEstadosProceso(proceso.getCodProceso()).getData());
			} catch (Exception e) {
				return e.getMessage();
			}
			if (numActuaciones == total) {
				return "";
			}
		} else {
			return "";
		}

		int i = total - numActuaciones - 1;
		List<State> loteEstados = new ArrayList<State>();
		State[] arrayActualizados;
		int cantidad = 30;
		while (i >= 0) {

			State estado = new State();
			estado.setCodProceso(proceso.getCodProceso());
			estado.setNum(total - i - 1);
			String fecha = "";
			estado.setActuacion(driver.findElement(By.id(lblActuacion + i)).getText());
			estado.setAnotacion(driver.findElement(By.id(lblAnotacion + i)).getText());
			fecha = driver.findElement(By.id(lblFechaActuacion + i)).getText();
			estado.setFechaActuacion(formatFechaLikeActuacion(fecha));
			fecha = driver.findElement(By.id(lblFechaFin + i)).getText();
			estado.setFechaFinTermino(formatFechaLikeActuacion(fecha));
			fecha = driver.findElement(By.id(lblFechaInicio + i)).getText();
			estado.setFechaInicioTermino(formatFechaLikeActuacion(fecha));
			fecha = driver.findElement(By.id(lblFechaRegistro + i)).getText();
			estado.setFechaRegistro(formatFechaLikeActuacion(fecha));
			loteEstados.add(estado);
			if ((i + 1) % cantidad == 0) {
				try {
					arrayActualizados = new State[loteEstados.size()];
					arrayActualizados = loteEstados.toArray(arrayActualizados);
					processDao.saveProcessStateBatch(arrayActualizados);
					arrayActualizados = null;
					loteEstados.clear();
				} catch (Exception e) {
					return e.getMessage();
				}
			}
			i--;
		}
		if (!loteEstados.isEmpty()) {
			try {
				arrayActualizados = new State[loteEstados.size()];
				arrayActualizados = loteEstados.toArray(arrayActualizados);
				processDao.saveProcessStateBatch(arrayActualizados);
				loteEstados.clear();
			} catch (Exception e) {
				return e.getMessage();
			}
		}

		return "";
	}

	private State obtenerUltimoEstado() {
		int i = 0;
		State estado = new State();
		String fecha = "";
		estado.setActuacion(driver.findElement(By.id(lblActuacion + i)).getText());
		estado.setAnotacion(driver.findElement(By.id(lblAnotacion + i)).getText());
		fecha = driver.findElement(By.id(lblFechaActuacion + i)).getText();
		estado.setFechaActuacion(formatFechaLikeActuacion(fecha));
		fecha = driver.findElement(By.id(lblFechaFin + i)).getText();
		estado.setFechaFinTermino(formatFechaLikeActuacion(fecha));
		fecha = driver.findElement(By.id(lblFechaInicio + i)).getText();
		estado.setFechaInicioTermino(formatFechaLikeActuacion(fecha));
		fecha = driver.findElement(By.id(lblFechaRegistro + i)).getText();
		estado.setFechaRegistro(formatFechaLikeActuacion(fecha));

		return estado;
	}

	private State haCambiadoEstadoActual(State estadoActual) {
		State estado = obtenerUltimoEstado();
		if (!estadoActual.equals(estado)) {
			return estado;
		}
		return null;
	}

	private ProcessInfo obtenerInfoProceso() {
		Boolean isPresent = driver.findElements(By.id("ClasificacionProceso")).size() > 0;
		if (isPresent) {
			ProcessInfo info = new ProcessInfo();
			info.setDespacho(driver.findElement(By.id("lblJuzgadoActual")).getText());
			info.setPonente(driver.findElement(By.id("lblPonente")).getText());

			ProcessClass clasificacion = new ProcessClass();
			clasificacion.setTipo(driver.findElement(By.id("lblTipo")).getText());
			clasificacion.setClase(driver.findElement(By.id("lblClase")).getText());
			clasificacion.setRecurso(driver.findElement(By.id("lblRecurso")).getText());
			clasificacion.setUbicacion(driver.findElement(By.id("lblUbicacion")).getText());
			info.setClasificacion(clasificacion);
			List<String> input = new ArrayList<String>(
					Arrays.asList(driver.findElement(By.id("lblNomDemandante")).getText().split("\n")));
			List<String> demandantes = new ArrayList<String>();
			List<String> demandados = new ArrayList<String>();
			for (int i = 0; i < input.size(); i++) {
				if (input.get(i).length() < 2) {
					continue;
				}
				if (input.get(i).contains("-")) {
					demandantes.add(input.get(i).substring(2));					
				}else {
					demandantes.add(input.get(i));
				}
			}
			info.setDemandantes(demandantes);
			input = new ArrayList<String>(
					Arrays.asList(driver.findElement(By.id("lblNomDemandado")).getText().split("\n")));
			for (int i = 0; i < input.size(); i++) {
				if (input.get(i).length() < 2) {
					continue;
				}
				if (input.get(i).contains("-")) {
					demandados.add(input.get(i).substring(2));					
				}else {
					demandados.add(input.get(i));
				}
			}
			info.setDemandados(demandados);

			info.setContenido(driver.findElement(By.id("lblContenido")).getText());
			return info;

		}
		return null;
	}

	private String revisarInfo(Process proceso) {
		try {
			ProcessInfo info = obtenerInfoProceso();
			if (!info.equals(null)) {
				if (!info.equals(proceso.getInfo())) {
					proceso.setInfo(info);
					proceso.infoActualizada();
				}
			}
		} catch (Exception e) {
			if (!proceso.getRepetirScan()) {
				proceso.setRepetirScan(true);
			}
			return "Error obteniendo la informaci�n. " + e.getMessage();
		}
		return "";
	}

	private String revisarUltimoEstado(Process proceso) {
		try {
			boolean esNuevoProceso = proceso.getEstadoActual() == null;
			State nuevoEstado = haCambiadoEstadoActual(proceso.getEstadoActual());
			if (nuevoEstado != null) {
				proceso.setEstadoActual(nuevoEstado);
				if (!esNuevoProceso) {
					proceso.estadoActualizado();
				}
				Date d = nuevoEstado.parseFechaActuacion();
				proceso.setUltimaAct(d);
			}
		} catch (Exception e) {
			return "Error actualizando el �ltimo estado. " + e.getMessage();
		}
		return "";
	}

	private void reload() {
		driver.get(webPage);
	}

	private void goForNuevaConsulta() {
		WebElement we = driver.findElement(By.id(btnNuevaConsulta));
		;
		String display = we.getCssValue("display");
		if (!display.equals("none") && !display.equals("run-in") && !display.equals("contents") && we.isDisplayed()) {
			we.click();
		}
	}

	private void waitUntilSelectOptionsPopulated(final Select select) {
		new FluentWait<WebDriver>(driver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(10, TimeUnit.MILLISECONDS)
				.until(new Predicate<WebDriver>() {
					public boolean apply(WebDriver d) {
						return (select.getOptions().size() > 0);
					}
				});
	}

	private static WebElement waitForElements(WebDriver driver, By by) {
		return (new WebDriverWait(driver, 10)).until(ExpectedConditions.presenceOfElementLocated(by));
	}

	private static Boolean waitForModalError(WebDriver driver) {
		try {
			return (new WebDriverWait(driver, 4)).until(MODAL_ERROR_SHOWS);
		} catch (Exception e) {
			return false;
		}
	}

	private static Boolean waitForResults(WebDriver driver) {
		try {
			return (new WebDriverWait(driver, 40)).until(DIV_ACTUACIONES_SHOW);
		} catch (Exception e) {
			return false;
		}
	}

	private boolean waitForPageLoad(WebDriver driver, int waitTimeInSec, ExpectedCondition<Boolean>... conditions) {
		boolean isLoaded = false;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(waitTimeInSec, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class).pollingEvery(2, TimeUnit.SECONDS);
		for (ExpectedCondition<Boolean> condition : conditions) {
			isLoaded = wait.until(condition);
			if (isLoaded == false) {
				// Stop checking on first condition returning false.
				break;
			}
		}
		return isLoaded;
	}

	/**
	 * Returns 'true' if the value of the 'window.document.readyState' via
	 * JavaScript is 'complete'
	 */
	private static final ExpectedCondition<Boolean> EXPECT_DOC_READY_STATE = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			String script = "if (typeof window != 'undefined' && window.document) { return window.document.readyState; } else { return 'notready'; }";
			Boolean result;
			try {
				result = ((JavascriptExecutor) driver).executeScript(script).equals("complete");
			} catch (Exception ex) {
				result = Boolean.FALSE;
			}
			return result;
		}
	};

	private static final ExpectedCondition<Boolean> MODAL_ERROR_SHOWS = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			WebElement inputt = driver.findElement(By.id("modalError"));

			String display = inputt.getCssValue("display");
			return !display.equals("none") && !display.equals("run-in") && !display.equals("contents");
		}
	};

	private static final ExpectedCondition<Boolean> DIV_ACTUACIONES_SHOW = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			WebElement inputt = driver.findElement(By.id("panelActuaciones"));

			String display = inputt.getCssValue("display");
			return !display.equals("none") && !display.equals("run-in") && !display.equals("contents");
		}
	};

	private static String formatFechaLikeActuacion(String input) {
		input = input.replace("  ", "");
		if (input.isEmpty()) {
			return "";
		}
		DateFormat format = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
		DateFormat outputFormat = new SimpleDateFormat("dd MMM YYYY", Locale.getDefault());
		Date date;
		try {
			date = format.parse(input);
			return outputFormat.format(date);
		} catch (ParseException e) {
			return input.toLowerCase();
		}
	}

	private int getEntidadIndexInSelect(String ciudadID, String entidadValue) {
		if (this.mapaEntidades.containsKey(ciudadID)) {
			String value = getIdEntidadByValue(entidadValue);
			return this.mapaEntidades.get(ciudadID).get(value);
		}
		return 0;
	}

	private void cargarListaOpciones(String ciudadID, List<WebElement> options) {
		if (!this.mapaEntidades.containsKey(ciudadID)) {
			Map<String, Integer> entidades = new HashMap<String, Integer>();
			int index = 0;
			for (WebElement op : options) {
				String id = getIdEntidadByValue(op.getAttribute("value"));
				entidades.put(id, index++);
				// 261-True-0306-11001-Consejo de Estado-Sala de Consulta y
				// Servicio Civil-False
				// 262-True-0315-11001-Consejo de Estado-Secretaria
				// General-False
				// 263-True-0331-11001-Consejo de Estado-SIN
				// SECCIONES/ESCRITURAL-False
			}
			this.mapaEntidades.put(ciudadID, entidades);
		}
	}

	private String getIdEntidadByValue(String valueEntidad) {
		try {
			return valueEntidad.substring(0, valueEntidad.indexOf("-"));
		} catch (Exception e) {
			return null;
		}
	}

	public Message<String> validarProceso(WebDriver wd, Process proceso) {
		driver = wd;
		driver.get(webPage);
		PageUtils.waitForPageLoad(driver);
		PageUtils.waitForElement(driver, By.id(campoCiudad), 3);

		cargarDatos(proceso);
		if (proceso.getProblem() != null || proceso.getAlerta() != null) {
			return new Message<>(false, proceso.getProblem().getMessage());
		}
		return new Message<>(true, "OK");
	}
}
