package com.absoft.services;

//import j.antigate.CapchaBypass;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NuevaConsulta {
	static String ciudad = "BOGOTA, D.C.";
	static String entidad = "499-True-3333-11001-Juzgado Administrativo-Oralidad";
	static String tipoConsulta = "1"; // N�mero de Radicaci�n
	static String idProceso = "11001333100320070018600";
	
	// +++++++++++++++++++++++
	private static String host = "api.anti-captcha.com";
    private static String clientKey = "a485f4af52aba5004c09ab09bc87413b4";

	public static void main(String[] args) {

		// String chromePath =
		// "C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe";
		// System.setProperty("webdriver.chrome.driver", chromePath);

		WebDriver driver = new FirefoxDriver();

		driver.manage().window().setSize(new Dimension(1024, 768));

		driver.get("https://procesojudicial.ramajudicial.gov.co/Justicia21/Administracion/Ciudadanos/frmConsulta");

		Select select;
		WebElement inputt;
		WebElement button;
		// Close modal
		
		inputt = driver.findElement(By.id("myModal"));
		
		String display = inputt.getCssValue("display");
		if (!display.equals("none") && !display.equals("run-in") && !display.equals("contents")) {
			button = inputt.findElement(By.xpath("//*[@id='myModal']//button"));
			button.click();
		}
		
		
		
		inputt = driver.findElement(By.id("MainContent_txtCodigoProceso"));
		inputt.sendKeys(idProceso);
		Random rand = new Random();
		
		//***** CAPTCHA ZONE
		WebElement captchaEle = driver.findElement(By.id("MainContent_imgCaptcha"));
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String captcha = "";
	
		try {
			BufferedImage bi = ImageIO.read(scrFile);
			
			// Get the location of element on the page
			Point point = captchaEle.getLocation();
			
			// Get width and height of the element
			int eleWidth = captchaEle.getSize().getWidth();
			int eleHeight = captchaEle.getSize().getHeight();

			// Crop the entire page screenshot to get only element screenshot
			BufferedImage eleScreenshot= bi.getSubimage(point.getX(), point.getY(),
			    eleWidth, eleHeight);
			String path = "C:\\Users\\JC Achury\\Desktop\\captcha" + rand.nextInt(100) + ".png";
			File cImage = new File(path);
			ImageIO.write(eleScreenshot, "png", cImage);
			// CALL Captcha breaker service
			FileInputStream is = new FileInputStream(cImage);
//			captcha = CapchaBypass.CapchaAnswer(is, clientKey, null, null,null);
			captcha = "";
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		//****** END OF CAPTCHA ZONE
		
		
//		Scanner scan = new Scanner(System.in);
//		System.out.println("Insert captcha");
//		captcha = scan.next().toUpperCase();
		
		inputt = driver.findElement(By.id("MainContent_txtCaptchaText"));
		inputt.sendKeys(captcha);
		
		button = driver.findElement(By.xpath("//input[@value='Consultar' and @type='submit']"));
		button.click();
		
		driver.close();
		driver.quit();

	}

	public static WebElement waitForElements(WebDriver driver, By by) {
		return (new WebDriverWait(driver, 10)).until(ExpectedConditions
				.presenceOfElementLocated(by));
	}

	public boolean waitForPageLoad(WebDriver driver, int waitTimeInSec,
			ExpectedCondition<Boolean>... conditions) {
		boolean isLoaded = false;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(waitTimeInSec, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class)
				.pollingEvery(2, TimeUnit.SECONDS);
		for (ExpectedCondition<Boolean> condition : conditions) {
			isLoaded = wait.until(condition);
			if (isLoaded == false) {
				// Stop checking on first condition returning false.
				break;
			}
		}
		return isLoaded;
	}

	/**
	 * Returns 'true' if the value of the 'window.document.readyState' via
	 * JavaScript is 'complete'
	 */
	public static final ExpectedCondition<Boolean> EXPECT_DOC_READY_STATE = new ExpectedCondition<Boolean>() {
		public Boolean apply(WebDriver driver) {
			String script = "if (typeof window != 'undefined' && window.document) { return window.document.readyState; } else { return 'notready'; }";
			Boolean result;
			try {
				result = ((JavascriptExecutor) driver).executeScript(script)
						.equals("complete");
			} catch (Exception ex) {
				result = Boolean.FALSE;
			}
			return result;
		}
	};
	private static void sendPost() throws Exception {

		String url = host;
		URL obj = new URL(url);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		String urlParameters = "key=" + clientKey;

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());

	}

}
