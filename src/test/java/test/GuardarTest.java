package test;

import java.io.File;
import java.util.List;

import org.openqa.selenium.WebDriver;

import com.absoft.core.VigileBotApp;
import com.absoft.model.Process;
import com.absoft.services.ConsultarProcesosSeleniumService;
import com.absoft.services.DriverUtils;
import com.absoft.services.db.ProcessDao;
import com.absoft.services.db.express.ProcessService;

public class GuardarTest {

	public static void main(String[] args) {
//		VigileBotApp.init();

		ProcessDao processDao = new ProcessService();

		List<Process> procesos = processDao.getProcesosByCompanyId("gFLBnLcFqF", 0, 30);
		ConsultarProcesosSeleniumService selenium = new ConsultarProcesosSeleniumService(procesos);
		WebDriver driver = DriverUtils.getWebDriver("chrome", new File("descargas"));
		selenium.iniciar(driver);

		driver.quit();

		Process[] arrayActualizados = new Process[procesos.size()];
		arrayActualizados = procesos.toArray(arrayActualizados);
		processDao.updateProcesos(arrayActualizados);
	}
}
